package com.example.androidapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.RequiresApi;

import java.io.IOException;
import java.net.ConnectException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Locale;
import java.util.Hashtable;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class GetMapObject implements KvmSerializable {
    public String map_bytes;

    public GetMapObject() {
    }

    public GetMapObject(String map_bytes, float current_zoom, float current_span_scale) {
        this.map_bytes = map_bytes;
    }

    @Override
    public Object getProperty(int arg0) {
        if (arg0 == 0) {
            return this.map_bytes;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {
        if (arg0 == 0) {
            arg2.type = PropertyInfo.VECTOR_CLASS;
            arg2.name = "map_bytes";
        }

    }

    @Override
    public void setProperty(int arg0, Object arg1) {
        if (arg0 == 0) {
            this.map_bytes = arg1.toString(); //.getBytes(StandardCharsets.UTF_8);
            // this.map_bytes = Base64.getDecoder().decode(arg1.toString());
        }

    }
}

class MarshalFloat implements Marshal {
    public Object readInstance(XmlPullParser parser, String namespace, String name,
                               PropertyInfo expected) throws IOException, XmlPullParserException {
        return Float.parseFloat(parser.nextText());
    }

    public void register(SoapSerializationEnvelope cm) {
        cm.addMapping(cm.xsd, "float", Float.class, this);
    }

    public void writeInstance(XmlSerializer writer, Object obj) throws IOException {
        writer.text(obj.toString());
    }
}

public class CallWebService extends AsyncTask<String, Void, String> {
    String NAMESPACE = "spyne.examples.hello.soap"; // The targetNamespace in WSDL.
    String METHOD_NAME = "get_map2"; // It is the method name in web service. We can have several methods.
    String SERVER_PORT = "8000";
    public final int WAIT_FOR = -1;
    public final int SUCCESS = 0;
    public final int CONNECTION_FAILED = 1;
    public final int OTHER = 2;

    String serverIp;
    float top;
    float bottom;
    float left;
    float right;
    ImageView mapView;
    GetMapObject resObj = new GetMapObject();
    int return_code = WAIT_FOR;

    CallWebService(String serverIp,
                   float top, float bottom, float left, float right,
                   ImageView mapView) {
        this.serverIp = serverIp;
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
        this.mapView = mapView;
    }

    @Override
    protected void onPostExecute(String s) {
        //String ss = (s == null) ? "Null ;/" : s;
        //Log.i("ProjectDebug", ss);
    }

    @Override
    protected String doInBackground(String... params) {
        String soapAction = String.format("http://%s:%s/%s/%s", this.serverIp, SERVER_PORT, NAMESPACE, METHOD_NAME);
        String url = String.format("http://%s:%s/?wsdl", this.serverIp, SERVER_PORT); // It is the url of WSDL file.
        String result = "";
        Log.i("ProjectDebug", "In Call");

        SoapObject soapObject = new SoapObject(NAMESPACE, METHOD_NAME);

        PropertyInfo top_ = new PropertyInfo();
        top_.setName("top");
        top_.setType(PropertyInfo.INTEGER_CLASS);
        top_.setValue(this.top);
        soapObject.addProperty(top_);

        PropertyInfo bottom_ = new PropertyInfo();
        bottom_.setName("bottom");
        bottom_.setType(PropertyInfo.INTEGER_CLASS);
        bottom_.setValue(this.bottom);
        soapObject.addProperty(bottom_);

        PropertyInfo left_ = new PropertyInfo();
        left_.setName("left");
        left_.setType(PropertyInfo.INTEGER_CLASS);
        left_.setValue(this.left);
        soapObject.addProperty(left_);

        PropertyInfo right_ = new PropertyInfo();
        right_.setName("right");
        right_.setType(PropertyInfo.INTEGER_CLASS);
        right_.setValue(this.right);
        soapObject.addProperty(right_);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(soapObject);
        envelope.addMapping(NAMESPACE, "GetMapObject", GetMapObject.class);
        // envelope.dotNet = true; no changes // default false
        MarshalFloat mf = new MarshalFloat();
        mf.register(envelope);

        HttpTransportSE httpTransportSE = new HttpTransportSE(url);
        Log.i("ProjectDebug", "after url");
        httpTransportSE.debug = true; // enables httpTransportSE.requestDump and httpTransportSE.responseDump
        // GetMapObject resObj = new GetMapObject();
        try {
            httpTransportSE.call(soapAction, envelope);
            Log.i("ProjectDebug", "call executed");
            SoapObject res = (SoapObject) envelope.getResponse();

            resObj.map_bytes = res.getProperty(0).toString();
//            byte[] decodeString = Base64.getDecoder().decode(resObj.map_bytes);
//            Bitmap decodedBytes = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
//            mapView.setVisibility(View.VISIBLE);
//            mapView.setImageBitmap(decodedBytes);
            // resObj.map_bytes = res.getProperty(0).toString().getBytes(StandardCharsets.UTF_8);
            Log.i("ProjectDebug", resObj.map_bytes);
            // Log.i("ProjectDebug", resObj.map_bytes.length);
            result = res.toString();
            Log.i("ProjectDebug", "cos sie dzieje");
            // SoapPrimitive soapPrimitive = (SoapPrimitive) envelope.getResponse();
            // result = soapPrimitive.toString();
            Log.i("ProjectDebug", result);
            Log.i("ProjectDebug", "cos sie stalo?");
            this.return_code = SUCCESS;
        } catch (ConnectException e) {
            // httpTransportSE.requestDump
            // httpTransportSE.responseDump
            Log.i("ProjectDebug", e.toString());
            this.return_code = CONNECTION_FAILED;
        } catch (Exception e) {
            e.printStackTrace();
            this.return_code = OTHER;
        }
        return resObj.map_bytes;
    }
}
