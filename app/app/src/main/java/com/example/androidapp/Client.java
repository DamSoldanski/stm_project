package com.example.androidapp;

public class Client {
    private final String _serverIp;
    private final String _url;
    private final String NAMESPACE = "http://vladozver.org/";
    private final String METHOD_NAME = "GetCategoryById";
    private final String SOAP_ACTION = "http://vladozver.org/GetCategoryById";
    private final String PARAMETER_NAME = "n";

    public Client(String server_ip) {
        _serverIp = server_ip;
        _url = String.format("http://%s/VipEvents/Services/CategoryServices.asmx", _serverIp);
    }

    /**
     * Returns an Image object from data received from the server.
     * @param longitude the longitude of the westernmost point (let's say max. left point)
     * @param latitude the latitude of the northernmost point (let's say max. upper point)
     * @param longitudeSpan longitude range
     * @param latitudeSpan latitude range
     */
    static void get(double longitude, double latitude, double longitudeSpan, double latitudeSpan) {
        //new CallWebService().execute("test_cos_tam");
    }
}
