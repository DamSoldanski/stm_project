package com.example.androidapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity {
    private static final int START_TOP = 0;
    private static final int START_BOTTOM = 1000;
    private static final int START_LEFT = 0;
    private static final int START_RIGHT = 1000;
    AppCompatActivity mainActivity = this;
    boolean init_get = false;

    int maxAvailableBottom = START_BOTTOM;
    int maxAvailableRight = START_RIGHT;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button connect_btn = findViewById(R.id.button_try_to_connect);
        Button getPrevious = findViewById(R.id.button_get_previous);

        TextView topLeft = findViewById(R.id.top_left);
        TextView bottomRight = findViewById(R.id.bottom_right);
        TextView leftLon = findViewById(R.id.left_lon);
        TextView rightLon = findViewById(R.id.right_lon);
        EditText topLatEdit = findViewById(R.id.top_lat_edit);
        EditText leftLonEdit = findViewById(R.id.left_lon_edit);
        EditText bottomLatEdit = findViewById(R.id.bottom_lat_edit);
        EditText rightLonEdit = findViewById(R.id.right_lon_edit);

        TextView textViewErrors = findViewById(R.id.text_view_errors);
        ImageView mapView = findViewById(R.id.imageView2);
        EditText editTextServerIp = findViewById(R.id.edit_text_server_ip);
        mapView.setVisibility(View.GONE);


        List<List<Integer>> states = new ArrayList<>();

//        DrawView drawView = findViewById(R.id.drawView1);
//        drawView.setVisibility(View.INVISIBLE);
        getPrevious.setOnClickListener(v -> {
            textViewErrors.setVisibility(View.INVISIBLE);
            Editable editableServerIp = editTextServerIp.getText();
            String serverIp = editableServerIp.toString();
            if (isValidInet4Address(serverIp)) {
                Log.i("ProjectDebug", "Trying to connect to " + serverIp);

                if (states.size() == 0) {
                    String errMsg = "Initial state is currently shown";
                    Log.i("ProjectDebug", errMsg);
                    textViewErrors.setText(errMsg);
                    textViewErrors.setVisibility(View.VISIBLE);
                    return;
                } else {
                    CallWebService cWS;
                    states.remove(states.size() - 1);
                    if (states.size() <= 1) getPrevious.setVisibility(View.INVISIBLE);
                    if (states.size() == 0) {
                        List<Integer> start_state = new ArrayList<>();
                        Collections.addAll(start_state, START_TOP, START_BOTTOM, START_LEFT, START_RIGHT);
                        states.add(start_state);
                        cWS = new CallWebService(
                                serverIp,
                                START_TOP, START_BOTTOM, START_LEFT, START_RIGHT,
                                mapView);
                    } else {
                        List<Integer> prev_state = states.get(states.size() - 1);
                        cWS = new CallWebService(
                                serverIp,
                                prev_state.get(0), prev_state.get(1), prev_state.get(2), prev_state.get(3),
                                mapView);
                    }

                    try {
                        cWS.execute("to be removed").get();
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (cWS.return_code == cWS.SUCCESS) {
                        textViewErrors.setText("Success");
                        textViewErrors.setVisibility(View.VISIBLE);
                        byte[] decodeString = Base64.getDecoder().decode(cWS.resObj.map_bytes);
                        Bitmap decodedBytes = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
                        hideSoftKeyboard(mainActivity);
                        mapView.setVisibility(View.VISIBLE);
                        mapView.setImageBitmap(decodedBytes);
                        showTextViews(topLeft, bottomRight, leftLon, rightLon,
                                topLatEdit, leftLonEdit, bottomLatEdit, rightLonEdit);
                        editTextServerIp.setVisibility(View.INVISIBLE);
                        maxAvailableBottom = decodedBytes.getHeight();
                        maxAvailableRight = decodedBytes.getWidth();
                        topLatEdit.setText(Integer.toString(START_TOP));
                        bottomLatEdit.setText(Integer.toString(maxAvailableBottom));
                        leftLonEdit.setText(Integer.toString(START_LEFT));
                        rightLonEdit.setText(Integer.toString(maxAvailableRight));

                    } else if (cWS.return_code == cWS.CONNECTION_FAILED) {
                        String errMsg = String.format("Cannot connect to server at '%s'", serverIp);
                        Log.i("ProjectDebug", errMsg);
                        textViewErrors.setText(errMsg);
                        textViewErrors.setVisibility(View.VISIBLE);
                    } else if (cWS.return_code == cWS.OTHER) {
                        String errMsg = "Unexpected error occurred";
                        textViewErrors.setText(errMsg);
                        textViewErrors.setVisibility(View.VISIBLE);
                    }

                }
            } else {
                String errMsg = String.format("'%s' is not a valid IPv4 address", editableServerIp);
                Log.i("ProjectDebug", errMsg);
                textViewErrors.setText(errMsg);
                textViewErrors.setVisibility(View.VISIBLE);
            }

        });

        connect_btn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                // textViewErrors.setText("a");
                textViewErrors.setVisibility(View.INVISIBLE);


                //mapView.setVisibility(View.GONE);
                // drawView.mapView = mapView;


                Editable editableServerIp = editTextServerIp.getText();
                String serverIp = editableServerIp.toString();

                if (isValidInet4Address(serverIp)) {
                    Log.i("ProjectDebug", "Trying to connect to " + serverIp);
                    CallWebService cWS;
                    if (!init_get) {
                        List<Integer> start_state = new ArrayList<>();
                        Collections.addAll(start_state, START_TOP, START_BOTTOM, START_LEFT, START_RIGHT);
                        states.add(start_state);

                        cWS = new CallWebService(
                                serverIp,
                                START_TOP, START_BOTTOM, START_LEFT, START_RIGHT,
                                mapView);

                    } else {
                        Log.i("ProjectDebug", "eeyy1");
                        int top, bottom, left, right;
                        List<Integer> new_state;
                        try {
                            top = Integer.parseInt(topLatEdit.getText().toString());
                            bottom = Integer.parseInt(bottomLatEdit.getText().toString());
                            left = Integer.parseInt(leftLonEdit.getText().toString());
                            right = Integer.parseInt(rightLonEdit.getText().toString());
                            if (top < START_TOP || bottom > maxAvailableBottom || left < START_LEFT || right > maxAvailableRight) {
                                textViewErrors.setText("Requested values are out of allowed values ranges!");
                                textViewErrors.setVisibility(View.VISIBLE);
                                topLatEdit.setText(Integer.toString(START_TOP));
                                bottomLatEdit.setText(Integer.toString(maxAvailableBottom));
                                leftLonEdit.setText(Integer.toString(START_LEFT));
                                rightLonEdit.setText(Integer.toString(maxAvailableRight));
                                return;
                            }
                            if (((top - bottom) >= 0) || ((left - right) >= 0)) {
                                textViewErrors.setText("Span cannot be less or equal 0!");
                                textViewErrors.setVisibility(View.VISIBLE);
                                topLatEdit.setText(Integer.toString(START_TOP));
                                bottomLatEdit.setText(Integer.toString(maxAvailableBottom));
                                leftLonEdit.setText(Integer.toString(START_LEFT));
                                rightLonEdit.setText(Integer.toString(maxAvailableRight));
                                return;
                            }
                            new_state = obtainPositions(states, top, bottom, left, right);
                            states.add(new_state);
                            // Collections.addAll(start_state, top, bottom, left, right);
                            // states.add(start_state);
                        } catch (NumberFormatException ex) {
                            textViewErrors.setText("Cannot convert value to integer number!");
                            textViewErrors.setVisibility(View.VISIBLE);
                            return;
                        }


                        cWS = new CallWebService(
                                serverIp,
                                new_state.get(0), new_state.get(1), new_state.get(2), new_state.get(3),
                                mapView);
                    }
                    try {
                        cWS.execute("to be removed").get();
                        Log.i("ProjectDebug", "after ex");
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }

                    Log.i("ProjectDebug", String.format(Integer.toString(cWS.return_code)));
                    if (cWS.return_code == cWS.SUCCESS) {
                        textViewErrors.setText("Success");
                        textViewErrors.setVisibility(View.VISIBLE);
                        Log.i("ProjectDebug", "after ex 1.1");
                        byte[] decodeString = Base64.getDecoder().decode(cWS.resObj.map_bytes);
                        Log.i("ProjectDebug", "after ex 1.2");
                        Bitmap decodedBytes = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.length);
                        Log.i("ProjectDebug", "after ex 1.2.1");
                        hideSoftKeyboard(mainActivity);
                        Log.i("ProjectDebug", "after ex 1.3");
                        mapView.setVisibility(View.VISIBLE);
                        mapView.setImageBitmap(decodedBytes);
                        Log.i("ProjectDebug", "after ex 2");
                        showTextViews(topLeft, bottomRight, leftLon, rightLon,
                                topLatEdit, leftLonEdit, bottomLatEdit, rightLonEdit);
                        editTextServerIp.setVisibility(View.INVISIBLE);
                        maxAvailableBottom = decodedBytes.getHeight();
                        maxAvailableRight = decodedBytes.getWidth();
                        topLatEdit.setText(Integer.toString(START_TOP));
                        bottomLatEdit.setText(Integer.toString(maxAvailableBottom));
                        leftLonEdit.setText(Integer.toString(START_LEFT));
                        rightLonEdit.setText(Integer.toString(maxAvailableRight));
                        if (init_get) getPrevious.setVisibility(View.VISIBLE);
                        init_get = true;
                        Log.i("ProjectDebug", "after ex3");
                    } else if (cWS.return_code == cWS.CONNECTION_FAILED) {
                        String errMsg = String.format("Cannot connect to server at '%s'", serverIp);
                        Log.i("ProjectDebug", errMsg);
                        textViewErrors.setText(errMsg);
                        textViewErrors.setVisibility(View.VISIBLE);
                    } else if (cWS.return_code == cWS.OTHER) {
                        String errMsg = "Unexpected error occurred";
                        textViewErrors.setText(errMsg);
                        textViewErrors.setVisibility(View.VISIBLE);
                    }

                } else {
                    String errMsg = String.format("'%s' is not a valid IPv4 address", editableServerIp);
                    Log.i("ProjectDebug", errMsg);
                    textViewErrors.setText(errMsg);
                    textViewErrors.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    public static boolean isValidInet4Address(String ip) {
        try {
            return Inet4Address.getByName(ip)
                    .getHostAddress().equals(ip);
        } catch (NetworkOnMainThreadException | UnknownHostException ex) {
            return false;
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        Log.i("ProjectDebug", "after ex 1.2.2");
        if (inputMethodManager.isAcceptingText()) {
            try {
                inputMethodManager.hideSoftInputFromWindow(
                        activity.getCurrentFocus().getWindowToken(),
                        0
                );
            } catch (Exception e) {
                // null object since keyboard is opened
                e.printStackTrace();
            }


        }
    }

    public static void hideTextViews(TextView... textViews) {
        for (TextView el : textViews) {
            el.setVisibility(View.INVISIBLE);
        }
    }

    public static void showTextViews(TextView... textViews) {
        for (TextView el : textViews) {
            el.setVisibility(View.VISIBLE);
        }
    }

    public static List<Integer> obtainPositions(List<List<Integer>> states, int top,
                                                int bottom, int left, int right) {
        int prev_top, prev_bottom, prev_left, prev_right;
        int new_top, new_bottom, new_left, new_right;
        List<Integer> new_state = new ArrayList<>();
        int prev_height, prev_width;
        if (states.size() == 0) {
            prev_top = START_TOP;
            prev_bottom = START_BOTTOM;
            prev_left = START_LEFT;
            prev_right = START_RIGHT;
        } else {
            List<Integer> prev_state = states.get(states.size() - 1);
            prev_top = prev_state.get(0);
            prev_bottom = prev_state.get(1);
            prev_left = prev_state.get(2);
            prev_right = prev_state.get(3);
        }
        prev_height = prev_bottom - prev_top;
        prev_width = prev_right - prev_left;
        float scale;
        if (prev_width >= prev_height) scale = (float) prev_width / 1000;
        else scale = (float) prev_height / 1000;
        new_top = prev_top + (int) (scale * top);
        new_bottom = prev_top + (int) (scale * bottom);
        new_left = prev_left + (int) (scale * left);
        new_right = prev_left + (int) (scale * right);
        Collections.addAll(new_state, new_top, new_bottom, new_left, new_right);
        return new_state;
        //states.add(start_state);
        // E e = list.get(list.size() - 1);
        // List<Integer> e = start_state.get(list.size() - 1);
    }
}