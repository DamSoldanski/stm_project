from suds.client import Client
import base64
import consts


class TestApp():
    def __init__(self, server_ip, latitude, longitude):
        server_url = f'http://{server_ip}:{consts.SERVER_PORT}'
        print(f'Looking for a server at {server_url}...')
        self.client = Client(f'{server_url}/?wsdl')
        print(f'Server available.')
        self.current_latitude = latitude
        self.current_longitude = longitude
        print("Initial 'get_map' call...")
        init_response = self.client.service.get_map(self.current_latitude, self.current_longitude)
        self.map_bytes = init_response.map_bytes.encode('ascii')
        self.current_zoom = init_response.current_zoom
        self.current_span_scale = init_response.current_span_scale
        print('ok')

        self.state_counter = 0

    def zoom(self, latitude, longitude, span):
        print("'get_map' call...")
        response = self.client.service.get_map(latitude, longitude, self.current_zoom, span, self.current_span_scale)

        self.current_latitude = latitude
        self.current_longitude = longitude

        self.map_bytes = response.map_bytes.encode('ascii')
        self.current_zoom = response.current_zoom
        self.current_span_scale = response.current_span_scale
        print('ok')

        self.state_counter += 1

    def zoom2(self, top_, bottom_, left_, right_):
        response = self.client.service.get_map2(
            top_, bottom_, left_, right_, self.current_zoom, self.current_span_scale)

    def show_state(self):
        print('Current state:')
        print(self._get_state())

    def _get_state(self):
        return f"""
                - latiutude\t - {self.current_latitude}
                - longitude\t - {self.current_longitude}
                - zoom\t\t - {self.current_zoom}
                - span_scale\t - {self.current_span_scale}
            """

    def save_state(self):
        state = self._get_state()
        info = state.strip().replace('\n', '').replace('\t', '').replace(' ', '').replace('-', '_')
        with open(f'state_{self.state_counter}{info}.png', 'wb') as file:
            file.write(base64.b64decode(self.map_bytes))


class TestApp2():
    def __init__(self, server_ip):
        server_url = f'http://{server_ip}:{consts.SERVER_PORT}'
        print(f'Looking for a server at {server_url}...')
        self.client = Client(f'{server_url}/?wsdl')
        print(f'Server available.')
        print("Initial 'get_map' call...")
        init_response = self.client.service.get_map2()
        self.map_bytes = init_response.map_bytes.encode('ascii')
        self.states = []
        self.state_counter = 0

    def _obtain_positions(self, top, bottom, left, right):
        try:
            prev_top, prev_bottom, prev_left, prev_right = self.states[-1]
            prev_width = prev_right - prev_left
            prev_height = prev_top - prev_bottom
        except IndexError:
            prev_top, prev_bottom, prev_left, prev_right = 0, 1000, 0, 1000
            prev_width = prev_right - prev_left
            prev_height = prev_top - prev_bottom

        scale = prev_width / 1000 if prev_width >= prev_height else prev_height / 1000
        new_top = prev_top + int(scale * top)
        new_bottom = prev_top + int(scale * bottom)
        new_left = prev_left + int(scale * left)
        new_right = prev_left + int(scale * right)
        return new_top, new_bottom, new_left, new_right


    def get(self, top, bottom, left, right):
        print("'get_map' call...")
        top_, bottom_, left_, right_ = self._obtain_positions(top, bottom, left, right)
        self.states.append((top_, bottom_, left_, right_))
        response = self.client.service.get_map2(*self.states[-1])
        self.map_bytes = response.map_bytes.encode('ascii')
        print('ok')
        self.state_counter += 1

    def previous(self):
        print("'get_map' call...")
        try:
            self.states.pop(-1)
            response = self.client.service.get_map2(*self.states[-1])
        except IndexError:
            response = self.client.service.get_map2()
        self.map_bytes = response.map_bytes.encode('ascii')
        print('ok')
        self.state_counter += 1

    def show_state(self):
        print('Current state:')
        print(self._get_state())

    def _get_state(self):
        try:
            curr_state = self.states[-1]
        except IndexError:
            curr_state = 0, 1000, 0, 1000
        return f"""
                - top\t\t - {curr_state[0]}
                - bottom\t - {curr_state[1]}
                - left\t\t - {curr_state[2]}
                - right\t\t - {curr_state[3]}
            """

    def save_state(self):
        state = self._get_state()
        info = state.strip().replace('\n', '').replace('\t', '').replace(' ', '').replace('-', '_')
        with open(f'state_{self.state_counter}{info}.png', 'wb') as file:
            file.write(base64.b64decode(self.map_bytes))


if __name__ == "__main__":
    client = TestApp2(consts.SERVER_IP)
    client.show_state()
    client.save_state()
    client.get(400, 600, 200, 800)
    client.show_state()
    client.save_state()
    client.get(50, 150, 200, 800)
    client.show_state()
    client.save_state()
    client.previous()
    client.show_state()
    client.save_state()
