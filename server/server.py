#!/usr/bin/env python
# encoding: utf8
#
# Copyright © Burak Arslan <burak at arskom dot com dot tr>,
#             Arskom Ltd. http://www.arskom.com.tr
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#    2. Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#    3. Neither the name of the owner nor the names of its contributors may be
#       used to endorse or promote products derived from this software without
#       specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


'''
This is a simple HelloWorld example to show the basics of writing
a webservice using spyne, starting a server, and creating a service
client.
Here's how to call it using suds:
>>> from suds.client import Client
>>> c = Client('http://localhost:7789/?wsdl')
>>> c.service.say_hello('punk', 5)
(stringArray){
   string[] =
      "Hello, punk",
      "Hello, punk",
      "Hello, punk",
      "Hello, punk",
      "Hello, punk",
 }
>>>
'''


import logging

from spyne.decorator import srpc
from spyne.service import ServiceBase
from spyne.model.complex import ComplexModel
from spyne.model.complex import Iterable
from spyne.model.primitive import Boolean, Decimal, Integer, Float
from spyne.model.primitive import Unicode
from spyne.model.binary import ByteArray

from spyne.util.simple import wsgi_soap_application

from consts import SERVER_IP, SERVER_PORT
from server_methods import handle_get_map, handle_get_map2


class GetMapObject(ComplexModel):
    # __namespace__ = 'spyne.examples.hello.soap' # required?

    map_bytes = ByteArray
    current_zoom = Float
    current_span_scale = Float


class GetMapObject2(ComplexModel):
    # __namespace__ = 'spyne.examples.hello.soap' # required?

    map_bytes = ByteArray


class SomeObject(ComplexModel):
    __namespace__ = 'spyne.examples.hello.soap'

    i = Integer
    s = Unicode


class HelloWorldService(ServiceBase):
    __out_header__ = SomeObject

    #start_map = get_start_map()

    @srpc(Unicode, _returns=Unicode)
    def just_string(s='test'):
        return s + s

    @srpc(Unicode, Integer, _returns=Iterable(Unicode))
    def say_hello(name, times):
        '''
        Docstrings for service methods appear as documentation in the wsdl
        <b>what fun</b>
        @param name the name to say hello to
        @param the number of times to say hello
        @return the completed array
        '''

        for i in range(times):
            yield f'Hello, {name}'

    @srpc(Float, Float, Float, Float, Float, _returns=GetMapObject)
    def get_map(latitude, longitude, current_zoom=None, span=None, current_span_scale=None):
        '''
        @param longitude the longitude of the westernmost point (let's say max. left point)
        @param latitude the latitude of the northernmost point (let's say max. upper point)
        @param current_zoom zoom level that was recently used
        @param span requested coords range
        @param current_span_scale scale that was used to fit recently got image to consts.MAP_SIZE * consts.SCALE size

        current_zoom and current_span_scale returned from each get_map method call
        should be kept by the client and used as parameters for next call - server is designed to be stateless

        @return bytes encoded in byte64 representing pixels of map - based on given parameters
        '''
        print(f"get_map request params:")
        print(f"{latitude=}")
        print(f"{longitude=}")
        print(f"{current_zoom=}")
        print(f"{span=}")
        print(f"{current_span_scale=}")
        return handle_get_map(latitude, longitude, current_zoom, span, current_span_scale)

    @srpc(Integer, Integer, Integer, Integer, _returns=GetMapObject2)
    def get_map2(top=None, bottom=None, left=None, right=None):
        '''
        @param top: max. upper point [px]
        @param bottom: max. bottom point [px]
        @param left: max. left point [px]
        @param right: max. right point [px]

        @return complex model cointaining 'map_bytes': bytes encoded in byte64 representing pixels of map - based on given parameters
        '''
        print(f"get_map request params:")
        print(f"{top=}")
        print(f"{bottom=}")
        print(f"{left=}")
        print(f"{right=}")
        return handle_get_map2(top, bottom, left, right)

#start_map = get_start_map()

if __name__ == '__main__':
    from wsgiref.simple_server import make_server

    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)

    logging.info(f"listening to http://{SERVER_IP}:{SERVER_PORT}")
    logging.info(f"wsdl is at: http://localhost:{SERVER_PORT}/?wsdl")

    wsgi_app = wsgi_soap_application([HelloWorldService], 'spyne.examples.hello.soap')
    server = make_server(SERVER_IP, SERVER_PORT, wsgi_app)
    server.serve_forever()
