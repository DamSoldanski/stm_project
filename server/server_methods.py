import base64
import io
import requests
from PIL import Image

from consts import INITIAL_ZOOM, MAP_SIZE, SCALE, STATIC_MAP_URL, CITY_LATITUDE, CITY_LONGITUDE
from helpers import Map, get_new_zoom_and_scale_for_zoom_and_span
from key import api_key

counter = iter(range(100))  # TODO remove in future


class ApiKeyMissingError(Exception):
    pass


def handle_get_map(latitude, longitude, current_zoom, span, span_scale):
    if api_key is None:
        return 1  # some http code needed, 5XX probably

    if current_zoom is None:
        zoom, current_zoom, span_scale = INITIAL_ZOOM, INITIAL_ZOOM, 1
    else:  # if not initial, then span has to be provided
        if span is None:
            raise ValueError("'span' is missing")
        if span_scale is None:
            raise ValueError("'span_scale' is missing")
        zoom, scale = get_new_zoom_and_scale_for_zoom_and_span(current_zoom, span)

    map_ = Map(latitude, longitude, zoom)
    print(map_)

    url = STATIC_MAP_URL + \
        'center=' + f'{latitude}, {longitude}' + \
        '&zoom=' + f'{zoom}' + \
        '&size=' + f'{MAP_SIZE}x{MAP_SIZE}' + \
        '&scale=' + f'{SCALE}' + \
        '&key=' + api_key
    response = requests.get(url)

    if span is None:
        with open(f'working{next(counter)}.png', 'wb') as file:
            base64_bytes = base64.b64encode(response.content)  # it is possible to decode base64
            file.write(base64.b64decode(base64_bytes))  # this image works fine
        print(f"INIT RETURN:")
        print(f"{current_zoom=}")
        print(f"{span_scale=}")
        return response.content, current_zoom, span_scale

    print(f'{zoom=}')
    print(f'{current_zoom=}')
    real_span = span / span_scale
    span_zoomed = real_span * (2**(zoom - current_zoom))
    span_scale = MAP_SIZE * SCALE / span_zoomed
    print(f'{span_zoomed=}')

    img = Image.open(io.BytesIO(response.content))
    print(f'{img.width=}, {img.height=}')

    (left, upper, right, lower) = (MAP_SIZE - span_zoomed / 2, MAP_SIZE - span_zoomed / 2, MAP_SIZE + span_zoomed / 2, MAP_SIZE + span_zoomed / 2)
    print(f'{left=} {upper=} {right=} {lower=}')
    img = img.crop((left, upper, right, lower))
    print(f'cropped {img.width=}, {img.height=}')

    img = img.resize(size=(MAP_SIZE * SCALE, MAP_SIZE * SCALE))
    print(f'resize {img.width=}, {img.height=}')

    img.save(f'working{next(counter)}.png')

    img_byte_array = io.BytesIO()
    img.save(img_byte_array, format='PNG')
    print(f"NOT INIT RETURN:")
    print(f"{zoom=}")
    print(f"{span_scale=}")
    return img_byte_array.getvalue(), zoom, span_scale


def get_start_map():
    if api_key is None:
        raise ApiKeyMissingError('Missing Google Maps API key on the server side')
    url = STATIC_MAP_URL + \
        'center=' + f'{CITY_LATITUDE}, {CITY_LONGITUDE}' + \
        '&zoom=' + f'{INITIAL_ZOOM}' + \
        '&size=' + f'{MAP_SIZE}x{MAP_SIZE}' + \
        '&scale=' + f'{SCALE}' + \
        '&key=' + api_key
    print("Start request done")
    return requests.get(url).content

start_map = get_start_map()

def handle_get_map2(top: int, bottom: int, left: int, right: int):
    args_not_existance = [x is None for x in (top, bottom, left, right)]

    if all(args_not_existance):
        return start_map,

    if any(args_not_existance):
        raise ValueError('not all arguments provided')
    width = right - left
    height = bottom - top
    if height <= 0:
        raise ValueError('top-bottom values error')
    if width <= 0:
        raise ValueError('right-left values error')

    img = Image.open(io.BytesIO(start_map))

    img = img.crop((left, top, right, bottom))

    if width > height:
        img = img.resize(size=(MAP_SIZE * SCALE, int(MAP_SIZE * SCALE * height / width)))
    else:
        img = img.resize(size=(int(MAP_SIZE * SCALE * width / height), MAP_SIZE * SCALE))

    img_byte_array = io.BytesIO()
    img.save(img_byte_array, format='PNG')
    return img_byte_array.getvalue(),
