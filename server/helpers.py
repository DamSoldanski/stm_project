import consts
import math
earth_radius = 6378137
DEFAULT_TILE_SIZE = 256
initialResolution = 2 * math.pi * earth_radius / DEFAULT_TILE_SIZE
# 156543.03392804062 for tileSize 256 pixels
originShift = 2 * math.pi * earth_radius / 2.0
# 20037508.342789244


def resolution(zoom):
    # for 1px^2
    return initialResolution / (2**zoom)


def pixels_to_meters(px, zoom):
    res = resolution(zoom)
    return px * res


def LatLonToMeters(lat, lon):
    "Converts given lat/lon in WGS84 Datum (degree) to XY (meter) in Spherical Mercator EPSG:900913"
    ""
    print(lat)
    print(lon)
    mx = lon * originShift / 180.0
    # check the tangens domain
    my = math.log(math.tan((90 + lat) * math.pi / 360.0)) / (math.pi / 180.0)

    my = my * originShift / 180.0
    return mx, my


def MetersToPixels(mx, my, zoom):
    "mx and my in meters"
    "Converts EPSG:900913 to pyramid pixel coordinates in given zoom level"
    "Gives the pixel NUMBER on the map"
    res = resolution(zoom)
    # print(f'{originShift=}')
    # print(f'{res=}')
    px = (mx + originShift) / res
    px = mx / res  # ???
    print('____________')
    print(f'{mx=}')
    print(f'{my=}')
    print(f'{zoom=}')
    print(f'{px=}')
    py = (my + originShift) / res
    py = my / res  # ?
    print(f'{py=}')
    print(f'___________')
    return px, py


# Dont forget you have to convert your projection to EPSG:900913
mx = -8_237_494.4864285  # -73.998672
my = 4_970_354.7325767  # 40.714728
zoom = 12

coords_ranges_per_zoom = {zoom: consts.MAP_SIZE / (2**zoom) for zoom in range(consts.INITIAL_ZOOM, 25)}
coords_res_per_zoom = {zoom: 1 / (2**zoom) for zoom in range(consts.INITIAL_ZOOM, 25)}
# for zoom, coords_range in coords_ranges_per_zoom.items():
#     print(zoom, coords_range)

# for zoom, coords_shift in coords_res_per_zoom.items():
#     print(zoom, coords_shift)

px = float


def get_new_zoom_and_scale_for_zoom_and_span(zoom: float, span: px):
    for z, res in coords_res_per_zoom.items():
        if consts.MAP_SIZE * consts.SCALE / span < coords_res_per_zoom[zoom] / res:
            break
        new_zoom = z
        zoom_diff = z - zoom
    scale = consts.MAP_SIZE / (span * (2**zoom_diff))
    print(f'zoom {zoom}; span {span}; new zoom {new_zoom}; scale {scale}; {2**zoom_diff*span*scale}')
    return new_zoom, scale


# zoom, scale = get_new_zoom_and_scale_for_zoom_and_span(10, 100)
# zoom, scale = get_new_zoom_and_scale_for_zoom_and_span(15, 40)
# print(zoom, scale)


def coords_per_pixel(zoom, map_scale=consts.SCALE, tale_size=DEFAULT_TILE_SIZE):
    '''Returns coordinatates span (in degree) for given zoom and map_scale'''
    # return 1 / (2**zoom) / consts.SCALE
    # return 180/tale_size/zoom/map_scale
    # return 180 / tale_size / 2**zoom / map_scale
    return tale_size / 180 / 2**zoom / map_scale  # bad?
    return 1 / 2**zoom / map_scale


class Map:
    class Point:
        def __init__(self, latitude, longitude):
            self.latitude = latitude
            self.longitude = longitude

        def __str__(self):
            formatted_lat = "{:.8f}".format(self.latitude)
            formatted_lon = "{:.8f}".format(self.longitude)
            return f'[{formatted_lat} {formatted_lon}]'

    def __init__(self, latitude_center, longitude_center, zoom, map_size=consts.MAP_SIZE, map_scale=consts.SCALE):
        coords_from_center_to_border = map_size / 2 / (2**zoom)
        coords_from_center_to_border = 256 / 180 * map_size / 2 / (2**zoom)  # to be verified
        print(coords_from_center_to_border*2, "_________")
        y_u = latitude_center + coords_from_center_to_border
        y_b = latitude_center - coords_from_center_to_border
        x_l = longitude_center - coords_from_center_to_border
        x_r = longitude_center + coords_from_center_to_border
        self.center = self.Point(latitude_center, longitude_center)
        self.upper_left = self.Point(y_u, x_l)
        self.upper_right = self.Point(y_u, x_r)
        self.bottom_left = self.Point(y_b, x_l)
        self.bottom_right = self.Point(y_b, x_r)
        self.zoom = zoom
        self.map_size = map_size
        self.scale = map_scale

    def __str__(self):
        return \
            f'''
    ===================================================================================
    Map:
        zoom: {self.zoom}
     ___________________________________<-{self.map_size*self.scale}px->_____________________________________
    |
    | Upper left:  {self.upper_left} \tUpper right:  {self.upper_right}
    |
    |\t\t\t\tCenter: {self.center} 
    |
    | Bottom left: {self.bottom_left} \tBottom right: {self.bottom_right}
    |__________________________________________________________________________________
    ===================================================================================
            '''


# map_ = Map(50, 20, 5)
# print(map_)
# map2 = Map(consts.CITY_LATITUDE, consts.CITY_LONGITUDE, consts.INITIAL_ZOOM)
# print(map2)
# print(coords_per_pixel(1))
# print(coords_per_pixel(5))
# print(coords_per_pixel(10))
# print(coords_per_pixel(20))
